export {};

declare module '@appsemble/sdk' {
  interface Messages {
    /**
     * The error message shown when the maximum amount of team members has been reached.
     */
    maxCapacityError: never;

    /**
     * The error message shown when the maximum amount of reservations
     * for the selected building has been reached.
     */
    maxReservationBuildingError: never;

    /**
     * The error message shown when the maximum amount of reservations
     * for the selected location has been reached.
     */
    maxReservationLocationError: never;

    /**
     * The error message shown when the maximum amount of reservations
     * for the selected room has been reached.
     */
    maxReservationRoomError: never;
  }

  interface Actions {
    /**
     * This action is used to determine the count of reservations on a day, time, and location.
     */
    count: never;

    /**
     * This action is used to determine the count of reservations on a day, time, and location
     * for the teams the user is a member of.
     */
    teamCount: never;

    /**
     * This is fired once when the location data is requested.
     */
    getLocations: never;

    /**
     * This action is used to fetch the user’s teams.
     */
    getTeams: never;
  }

  interface EventListeners {
    /**
     * This event will trigger the validation.
     */
    changed: never;

    /**
     * This event will trigger the team capacity validation.
     */
    timeChanged: never;

    /**
     * This event will trigger sending the list of locations for a building.
     */
    selectedBuilding: never;

    /**
     * This event will trigger sending the list of rooms for a location.
     */
    selectedLocation: never;

    /**
     * This event will validate all fields for validity.
     */
    validateAll: never;
  }

  interface EventEmitters {
    /**
     * This event is sent as an empty object
     * or an error depending on whether or not the reservation is valid.
     */
    validatedReservation: never;

    /**
     * This event is sent as an empty object
     * or an error depending on whether the team capacity validation is valid.
     */
    validatedTeam: never;

    /**
     * This event is sent with the input data
     * or an error depending on whether or not all fields are valid.
     */
    validatedAll: never;

    /**
     * An event containing enum options with the list of buildings.
     *
     * Emitted after loading.
     */
    buildings: never;

    /**
     * An event containing enum options with the list of locations based on the selected building.
     *
     * Emitted after selecting a different building.
     */
    locations: never;

    /**
     * An event containing enum options with the list of rooms based on the selected location.
     *
     * Emitted after selecting a different location.
     */
    rooms: never;
  }
}
