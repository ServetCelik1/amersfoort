import { bootstrap } from '@appsemble/sdk';

import { buildQuery, buildTeamQuery } from './odata';
import { Building, Reservation, Team } from './types';
import { findBuilding, findLocation, findRoom } from './utils';

bootstrap(({ actions, events, pageParameters, utils }) => {
  const buildingsPromise = actions.getLocations<Building[]>();
  const teamsPromise = actions
    .getTeams<Team[]>()
    .then((teams) => teams.filter((team) => team.role));

  buildingsPromise.then((locations) => {
    if (pageParameters?.id == null) {
      events.emit.rooms([]);
      events.emit.locations([]);
    }
    events.emit.buildings(
      [
        ...new Set(
          locations
            .filter((location) =>
              location.locations.some((loc) => loc.rooms.some((room) => room.capacity)),
            )
            .map((loc) => loc.buildingCode),
        ),
      ].map((code) => ({
        value: code,
        label: locations.find((loc) => loc.buildingCode === code)?.buildingName,
      })),
    );
  });

  /**
   * Validate whether there is room for reservations based on team attendance.
   *
   * @param data - The data containing the selected date and time.
   * @returns Remapper result containing an error response.
   */
  async function validateTeam({ date, id, time }: Reservation): Promise<string | undefined> {
    const teams = (await teamsPromise).map((team) => ({
      ...team,
      annotations: Object.fromEntries(
        Object.entries(team.annotations || {}).map(([key, value]) => [
          String(key).toLowerCase().replace(/\s/g, ''),
          value,
        ]),
      ),
    }));
    const lowestTeamCapacity = Math.min(
      ...teams
        .filter((team) => team.annotations?.capacity)
        .map((team) => {
          const size = team.annotations?.size ? Number(team.annotations.size) : team.size;
          return Math.ceil(Number(team.annotations.capacity) * size);
        }),
    );

    if (time === 3) {
      if (Number.isFinite(lowestTeamCapacity)) {
        const [morningTeamCount, afternoonTeamCount] = await Promise.all([
          actions.teamCount<number>({ query: buildTeamQuery(date, 1, id) }),
          actions.teamCount<number>({ query: buildTeamQuery(date, 2, id) }),
        ]);

        if (morningTeamCount < lowestTeamCapacity && afternoonTeamCount < lowestTeamCapacity) {
          return;
        }

        return utils.formatMessage('maxCapacityError');
      }

      return;
    }

    const count = await actions.teamCount<number>({ query: buildTeamQuery(date, time, id) });

    if (count < lowestTeamCapacity) {
      // Capacity not reached on the selected date and time.
      return;
    }

    return utils.formatMessage('maxCapacityError');
  }

  /**
   * Validates whether there is room in the specified room for another reservation.
   *
   * @param reservation - The reservation to validate the location for.
   * @returns Remapper result containing an error response.
   */
  async function validateLocation(reservation: Reservation): Promise<string | undefined> {
    const building = await findBuilding(buildingsPromise, reservation.building);
    const location = findLocation(building, reservation.location);
    const room = findRoom(location, reservation.room);

    let capacity = 0;
    let message = utils.formatMessage('maxReservationRoomError');

    if (building && !location && !room) {
      for (const loc of building.locations) {
        for (const r of loc.rooms) {
          capacity += r.capacity;
        }
      }
      message = utils.formatMessage('maxReservationBuildingError');
    } else if (building && location && !room) {
      for (const r of location.rooms) {
        capacity += r.capacity;
      }
      message = utils.formatMessage('maxReservationLocationError');
    } else if (building && location && room) {
      ({ capacity } = room);
    }

    // The conflict check for an entire day
    // requires different logic than that of a part of the day.
    if (reservation.time === 3) {
      const [morningCount, afternoonCount] = await Promise.all([
        actions.count<number>({ query: buildQuery(reservation, 1) }),
        actions.count<number>({ query: buildQuery(reservation, 2) }),
      ]);

      if (morningCount < capacity && afternoonCount < capacity) {
        // Capacity not reached on either the morning or afternoon of the day.
        return;
      }

      return message;
    }

    const count = await actions.count<number>({ query: buildQuery(reservation, 3) });

    if (count < capacity) {
      // Capacity not reached on the selected date, time, and location.
      return;
    }

    return message;
  }

  events.on.selectedBuilding(async (data: Reservation) => {
    const building = await findBuilding(buildingsPromise, data.building);
    const locations = building?.locations ?? [];

    events.emit.rooms([]);
    events.emit.locations(
      locations
        .filter((location) => location.rooms.some((room) => room.capacity))
        .map((location) => ({
          value: location.locationCode,
          label: location.locationName || location.locationCode,
        })),
    );
  });

  events.on.selectedLocation(async (data: Reservation) => {
    const building = await findBuilding(buildingsPromise, data.building);
    const location = findLocation(building, data.location);
    const rooms = location?.rooms ?? [];

    events.emit.rooms(
      rooms
        .filter((room) => room.capacity)
        .map((room) => ({
          value: room.roomCode,
          label: room.roomName || room.roomCode,
        })),
    );
  });

  events.on.validateAll(async (reservation: Reservation) => {
    const teamError = await validateTeam(reservation);

    if (teamError) {
      await events.emit.validatedAll(null, teamError);
      return;
    }

    const locationError = await validateLocation(reservation);
    if (locationError) {
      await events.emit.validatedAll(null, teamError);
      return;
    }

    const building = await findBuilding(buildingsPromise, reservation.building);
    const location = findLocation(building, reservation.location);
    const room = findRoom(location, reservation.room);

    await events.emit.validatedAll({
      ...reservation,
      locationName: location?.locationName,
      buildingName: building?.buildingName,
      roomName: room?.roomName,
    });
  });

  events.on.timeChanged(async (data: Reservation) => {
    const result = await validateTeam(data);
    await (result ? events.emit.validatedTeam(null, result) : events.emit.validatedTeam({}));
  });

  events.on.changed(async (data: Reservation) => {
    const result = await validateLocation(data);

    await (result
      ? events.emit.validatedReservation(null, result)
      : events.emit.validatedReservation({}));
  });
});
