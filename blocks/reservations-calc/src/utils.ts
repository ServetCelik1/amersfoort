import { Building, Location, Room } from './types';

/**
 * Find a building based on its code.
 *
 * @param buildingsPromise - The promise which resolves with all buildings.
 * @param buildingCode - The code of the building to find.
 * @returns The found building
 */
export async function findBuilding(
  buildingsPromise: Promise<Building[]>,
  buildingCode: string,
): Promise<Building | undefined> {
  const buildings = await buildingsPromise;
  return buildings.find((building) => building.buildingCode === buildingCode);
}

/**
 * Find the location in a given building.
 *
 * @param building - The building for which to find a location.
 * @param locationCode - The code of the location to find.
 * @returns The found location.
 */
export function findLocation(
  building: Building | undefined,
  locationCode: string,
): Location | undefined {
  return building?.locations.find((location) => location.locationCode === locationCode);
}

/**
 * Fint the room in a given location.
 *
 * @param location - The location for which to find a room.
 * @param roomCode - The code of the room to find.
 * @returns The found room.
 */
export function findRoom(location: Location | undefined, roomCode: string): Room | undefined {
  return location?.rooms.find((room) => room.roomCode === roomCode);
}
