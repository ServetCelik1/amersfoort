import { Reservation } from './types';

/**
 * Build an OData query to search for reservation conflicts.
 *
 * @param reservation - A reservation to create a conflict filter for.
 * @param time2 - Another time to search for.
 * @returns An OData query.
 */
export function buildQuery(
  { building, date, id, location, room, time }: Reservation,
  time2: Reservation['time'],
): string {
  const query = [`date eq '${date}'`, `(time eq '${time}' or time eq '${time2}')`];

  if (id != null) {
    query.push(`id ne ${id}`);
  }

  if (building) {
    query.push(`building eq '${building}'`);

    if (location) {
      query.push(`location eq '${location}'`);

      if (room) {
        query.push(`room eq '${room}'`);
      }
    }
  }

  return query.join(' and ');
}

/**
 * Build an OData query to search for reservation conflicts for teams.
 *
 * @param date - The date to search for.
 * @param time - A time to search for.
 * @param id - The ID of the resource that's being modified.
 * @returns An OData query.
 */
export function buildTeamQuery(date: string, time: Reservation['time'], id?: number): string {
  const query = `date eq '${date}' and (time eq '${time}' or time eq '3')`;
  return id == null ? query : `id ne ${id} and ${query}`;
}
