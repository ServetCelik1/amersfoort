// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Cypress {
  export interface Chainable {
    /**
     * Login to an Appsemble app.
     */
    login: () => void;

    /**
     * Helper function to wait until the app is loaded.
     */
    waitForAppLoaded: () => void;

    /**
     * Visit the app.
     */
    visitApp: () => void;
  }
}

Cypress.Commands.add('waitForAppLoaded', () => {
  cy.get('[data-block]').should('exist');
  cy.get('.appsemble-loader', { includeShadowDom: true, timeout: 8e3 }).should('not.exist');
});

Cypress.Commands.add('visitApp', () => {
  const url = String(Cypress.env('BASE_URL'));

  cy.visit(url);
  cy.waitForAppLoaded();
});

Cypress.Commands.add('login', () => {
  cy.session('app-login', () => {
    cy.visit(Cypress.env('BASE_URL'));
    cy.get('.appsemble-loader').should('not.exist');
    cy.get('.appsemble-login > button').click();
    cy.get('#email').type(Cypress.env('BOT_ACCOUNT_EMAIL'));
    cy.get('#password').type(Cypress.env('BOT_ACCOUNT_PASSWORD'));
    cy.get('button[type="submit"]').click();
    cy.get('.has-text-centered > .button.is-primary').click();
    cy.waitForAppLoaded();
  });
});
