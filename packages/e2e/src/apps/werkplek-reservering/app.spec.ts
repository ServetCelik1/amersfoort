describe('Reservation overview', () => {
  beforeEach(() => {
    cy.login();
    cy.visitApp();
  });

  it('should display a message if no reservations are available', () => {
    cy.intercept(
      { method: 'GET', url: '/api/apps/*/resources/reservations*' },
      {
        body: [],
      },
    );
    cy.contains('Reservations').should('exist');
    cy.contains('No reservations have been found.').should('exist');
  });

  it('should display the information page', () => {
    cy.contains('Information').click();
    cy.waitForAppLoaded();
    cy.contains('This app was created for the municipality of Amersfoort.').should('exist');
  });

  it('should be able to create and view a new reservation', () => {
    cy.contains(/^Reserve$/).click();
    cy.waitForAppLoaded();
    cy.get('#date').click().get('.flatpickr-day.today').click();
    cy.contains('Morning').click();
    cy.get('#building').should('be.enabled').select('A1 Name', { force: true });
    cy.get('#location').should('be.enabled').select('00F', { force: true });
    cy.get('#room').should('be.enabled').select('Room 0.007', { force: true });
    cy.get('#description').type('This is a test for the happy flow.', { force: true });
    cy.get('[type=submit]').click();

    const date = new Date();
    // Assert that the record exists
    cy.contains('A1 Name | 00F | Room 0.007').as('record').should('exist');
    cy.contains(`${date.getMonth() + 1}/${date.getDate()} | Morning`).should('exist');
    cy.get('@record').click();

    // Assert some details of the record in the modal
    cy.contains('This is a test for the happy flow.').should('exist');
    cy.contains('Name').should('exist');

    // Close the modal
    cy.get('.delete').click();

    // Click on the delete button in the dropdown menu
    cy.get('.fa-ellipsis-vertical').click();
    cy.contains('Delete').click();
    cy.get('.is-danger').click();
    cy.contains('No reservations have been found.').should('exist');
  });

  it('should display an error when the team capacity has been reached', () => {
    cy.contains(/^Reserve$/).click();
    cy.waitForAppLoaded();

    cy.get('#date').click().get('.flatpickr-day.today').click();
    cy.intercept(
      {
        url: '/api/apps/*/resources/reservation/$count*$team=member',
        method: 'GET',
        times: 1,
      },
      { body: 2 },
    );
    cy.contains('Morning').click();
    cy.contains('Team capacity was reached').should('exist');
    cy.contains('Afternoon').click();
    cy.contains('Team capacity was reached').should('not.exist');
  });

  it('should display an error when selecting a room without any space', () => {
    cy.contains(/^Reserve$/).click();
    cy.waitForAppLoaded();

    cy.get('#date').click().get('.flatpickr-day.today').click();
    cy.contains('Morning').click();
    cy.get('#building').should('be.enabled').select('E1 Name', { force: true });
    cy.get('#location').should('be.enabled').select('01F', { force: true });
    cy.get('#room').should('be.enabled').select('Room 1.015', { force: true });
    cy.contains('Room capacity was reached').should('exist');
    cy.get('#room').should('be.enabled').select('Room 1.023', { force: true });
    cy.contains('Room capacity was reached').should('not.exist');
  });

  it('should display an error when selecting a location without any space', () => {
    cy.contains(/^Reserve$/).click();
    cy.waitForAppLoaded();

    cy.get('#date').click().get('.flatpickr-day.today').click();
    cy.contains('Morning').click();
    cy.get('#building').should('be.enabled').select('E1 Name', { force: true });
    cy.intercept(
      {
        url: '/api/apps/*/resources/reservation/$count*E1*01*',
        method: 'GET',
      },
      { body: 1 },
    );
    cy.get('#location').should('be.enabled').select('01F', { force: true });
    cy.contains('Location capacity was reached').should('exist');
    cy.get('#location').should('be.enabled').select('00F', { force: true });
    cy.contains('Location capacity was reached').should('not.exist');
  });

  it('should display an error when selecting a building without any space', () => {
    cy.contains(/^Reserve$/).click();
    cy.waitForAppLoaded();

    cy.get('#date').click().get('.flatpickr-day.today').click();
    cy.contains('Morning').click();
    cy.intercept(
      {
        url: '/api/apps/*/resources/reservation/$count*E1*',
        method: 'GET',
      },
      { body: 18 },
    );
    cy.get('#building').should('be.enabled').select('E1 Name', { force: true });
    cy.contains('Building capacity was reached').should('exist');
    cy.get('#building').should('be.enabled').select('A3 Name', { force: true });
    cy.contains('Building capacity was reached').should('not.exist');
  });
});
