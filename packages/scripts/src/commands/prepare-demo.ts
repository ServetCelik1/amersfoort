import { readFile, rm, writeFile } from 'fs/promises';
import { join } from 'path';

import { logger, readData, writeData } from '@appsemble/node-utils';
import { AppsembleMessages } from '@appsemble/types';
import { merge } from 'lodash';
import { format, resolveConfig } from 'prettier';
import stripIndent from 'strip-indent';
import { parseDocument } from 'yaml';

export const command = 'prepare-demo';
export const description = 'Prepare the apps as demo apps by stripping Amersfoort information';

/**
 * Patch an app definition of an app inside the `apps` folder.
 *
 * @param name - The name of the app to patch.
 * @param patches - Patches to apply to the YAML.
 */
async function patchDefinition(
  name: string,
  patches: [key: (number | string)[], value: unknown][],
): Promise<void> {
  const path = join('apps', name, 'app-definition.yaml');
  logger.info(`Updating ‘${path}’`);
  const yaml = await readFile(path, 'utf-8');
  const doc = parseDocument(yaml);
  for (const [key, value] of patches) {
    doc.setIn(key, value);
  }
  const prettierOptions = (await resolveConfig(path, { editorconfig: true }))!;
  prettierOptions.parser = 'yaml';
  await writeFile(path, format(String(doc), prettierOptions));
  logger.info(`Succesfully updated ‘${path}’`);
}

/**
 * Patch app messages.
 *
 * The replacements will be deep merged into the original content. If no replacements are given, the
 * translations are deleted.
 *
 * @param name - The name of the app to patch.
 * @param language - The language of the messages to patch.
 * @param replacements - Replacements for the original messages.
 */
async function patchMessages(
  name: string,
  language: string,
  replacements?: Partial<AppsembleMessages>,
): Promise<void> {
  const path = join('apps', name, 'i18n', `${language}.json`);
  if (replacements) {
    const [messages] = await readData<AppsembleMessages>(path);
    await writeData(path, merge(messages, replacements));
  } else {
    await rm(path);
  }
}

/**
 * Strip any Amersfoort specific information from the `bezoekers` app.
 */
async function updateVisitors(): Promise<void> {
  await patchDefinition('bezoekers', [
    [['name'], 'Bezoekers'],
    [['description'], 'Deze app was gemaakt voor de gemeente Amersfoort.'],
    [['security', 'roles', 'Medewerker', 'description'], 'Medewerkers van de gemeente'],
    [
      ['resources', 'meeting', 'schema', 'properties', 'location', 'enum'],
      ['kerkstraat 12', 'parallelweg 3', 'molenweg 53', 'dorpsplein 4', 'sportlaan 1'],
    ],
    [['pages', 1, 'blocks', 0, 'parameters', 'fields', 1, 'defaultValue'], 'Kerkstraat 12'],
    [
      ['pages', 1, 'blocks', 0, 'parameters', 'fields', 1, 'enum'],
      [
        { label: 'Kerkstraat 12', value: 'kerkstraat 12' },
        { label: 'Parallelweg 3', value: 'parallelweg 3' },
        { label: 'Molenweg 53', value: 'molenweg 53' },
        { label: 'Dorpsplein 4', value: 'dorpsplein 4' },
        { label: 'Sportlaan 1', value: 'sportlaan 1' },
      ],
    ],
  ]);
  await patchMessages('bezoekers', 'nl', {
    app: {
      'app.roles.Medewerker.description': 'Medewerkers van de gemeente',
      description: 'Deze app was gemaakt voor de gemeente Amersfoort',
      name: 'Bezoekers',
    },
    messageIds: {
      emailBody: stripIndent(`
        Beste bezoeker,

        Hierbij bevestig ik uw afspraak op {datetime, date, long} om {datetime, time, ::k::m} op {location}.

        U heeft een afspraak met {visitedName}.

        Graag verzoeken we u om u te melden bij de receptie bij aankomst. Hier ontvangt u een bezoekersbadge die u na uw bezoek weer dient in te leveren bij de receptie.

        Met vriendelijke groet,\\
        De receptie
      `).trim(),
      emailSubject: 'Bevestiging afspraak',
    },
  });
  await patchMessages('bezoekers', 'en', {
    app: {
      'app.roles.Medewerker.description': 'Employees of the municipality',
      description: 'This app was created for the municipality of Amersfoort',
      name: 'Visitors',
    },
    messageIds: {
      emailBody: stripIndent(`
        Dear visitor,

        This is a confirmation for your appointment on {datetime, date, long} at {datetime, time, ::k::m} at {location}.

        You have an appointment with {visitedName}.

        We kindly request you to report at the reception on arrival. Here you will be given a visitor badge, which you should return afterwards.

        Kind regards,\\
        The reception
      `).trim(),
      emailSubject: 'Bevestiging afspraak',
    },
  });
}

/**
 * Strip any Amersfoort specific information from the `werkplek-reservering` app.
 */
async function patchWorkspacesReservation(): Promise<void> {
  await patchDefinition('werkplek-reservering', [[['description'], 'Reserveer uw werkblek']]);
  await patchMessages('werkplek-reservering', 'nl', {
    app: {
      description: 'Reserveer uw werkplek op kantoor',
    },
    messageIds: {
      information: stripIndent(`
        #### Over deze app

        Deze app was oorspronkelijk gemaakt voor de gemeente Amersfoort.
      `).trim(),
    },
  });
  await patchMessages('werkplek-reservering', 'en', {
    app: {
      description: 'Reserve your workspace at the office',
    },
  });
  await patchMessages('werkplek-reservering', 'da');
  await patchMessages('werkplek-reservering', 'fr');
}

/**
 * Prepare the apps as demo apps by stripping Amersfoort information.
 */
export async function handler(): Promise<void> {
  await updateVisitors();
  await patchWorkspacesReservation();
}
