module.exports = {
  root: true,
  extends: ['remcohaszing'],
  rules: {
    'import/no-unresolved': 'off',
  },
};
