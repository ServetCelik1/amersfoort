# Appsemble Amersfoort

> Apps and blocks for the municipality of Amersfoort

## References

- [Official Amersfoort style guide](https://www.amersfoort.nl/project/huisstijl-gemeente-amersfoort.htm)
- [Living document for progress/meetings](https://docs.google.com/document/d/1YZ0_xYvdL2KaTSxMcxSLIEqbrNiTvDZyUUZpI0dVfhg/edit)

### Bezoekers

- [Style guide](https://zeroheight.com/80e1745c2)
- [InVision](https://projects.invisionapp.com/d/main#/projects/prototypes/19766806) (Phone)
- [InVision](https://projects.invisionapp.com/d/main#/projects/prototypes/19934114) (Tablet)
